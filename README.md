# node-crypto-wrapper

## Module Usage

For one-line encryption of strings or buffers, using Node's built-in crypto functions.

    const cwrap = require('node-crypto-wrapper');

    let encryptedBuffer = cwrap.encryptBinary(Buffer, password);
    let decryptedBuffer = cwrap.decryptBinary(encryptedBuffer, password);

    let encryptedHexString = cwrap.encryptString("string", password);
    let decryptedString = cwrap.decryptString("hex string", password);

If no password is provided, it uses a user-specific key.

## CLI Usage

    node-crypto-wrapper <options> <input> <output>
      Options:
      <e: encrypt> | <d: decrypt>
      <f: from file> | <s: from string>
      <f: to file> | <s: to string>
      <p: password> | <u: user>

Examples:

    # Encrypts a file with a user key, decrypts it again
    $ node-crypto-wrapper effu plain.txt encrypted.txt
    $ node-crypto-wrapper dffu encrypted.txt plain_again.txt

    # Encrypts a string with a given key, decrypts it again
    $ node-crypto-wrapper essp
    $ node-crypto-wrapper dssp

Encrypted files are encoded as hex, and then written as text files - so any combination of options should work interchangeably.
You can encrypt a string directly to a file, without having to create a plaintext version of the file (or vice versa) if desired.

## Why?

The original use case is: "configuration files with passwords* in them"  
\* _I'm using "passwords" as shorthand for "anything sensitive you might put in a config file"_

The most popular answers I've seen are:
1. Build systems with their own secure password storage, injecting parameters at deploy time (Heroku, TeamCity, etc.)
2. Just put the config files on the production machine, so only people with administrator access can see them (hopefully plus encryption-at-rest for the disks)

I wrote and used this utility so that the config file could be encrypted for the application user, while assigning the developer-admins group the permissions necessary to go into the website's directory and do what they needed, there.

    $ node-crypto-wrapper esfu config.encrypted
    # enter JSON at the prompt
    
    -----
    
    const cwrap = require('node-crypto-wrapper');    
    const encryptedJson = fs.readFileSync('config.encrypted').toString();
    const configJson = cwrap.decryptString(encryptedJson);
    const config = JSON.parse(configJson);

##### Why not use one of these build systems for your passwords?

The original motivation was in dealing with a legacy system, where moving all of this to another CI platform wasn't a viable option.

However, generally with CI/build systems, administrators can read those passwords!
There are some conceivable situations in which you may not want a build admin to have all of the passwords that a production server needs...
Though, in general, I'd probably stick to just using your CI's built-in secrets manager.

##### Could I just use file permissions to control access? If the user's key is also, ultimately, stored on the machine and protected by file permissions, isn't it the same difference?

You could, but it's a lot easier to screw up permissions when you have one or two files in a directory that aren't readable by people who _can_ read the rest of the directory.
It could also cause cross-platform headaches, or trouble with other commands, like moving or copying files.

##### If someone has access to your production web server, aren't there tons of bad things they can do, anyway, even without those passwords? Likewise, if they have access to it indirectly via being a build admin?

Of course.
Nobody you don't trust should be able to log directly into a production server and do things without going through source control and code review.

But, there's still a level of risk mitigation between having an account that can be locked out in one place, and handing over a list of passwords, _each_ of which would have to be changed when a person left the company.

A malicious user who's got access to the website directory _could_ still write an API endpoint that dumped all of the configuration variables into a JSON response and then go hit that page...
I've met plenty of people that would _never ever_ do that, but just _might_ see a password in front of their face, copy/paste it over to their laptop so they could just run a test against real data, and forget they left it there. Haven't you?

One of my party guests _could_ take a mould of my house keys when I wasn't looking, but that's a different level of (mis)trust than handing out copies of my keys as party favors.

##### Could I also solve this with Active Directory? Just not have passwords in my config file in the first place?

Yes. But Microsoft solutions aren't hip or trendy.

Jokes aside, even if you were a 100% Microsoft shop, chances are you'll need an API from a third party at some point.

##### What's the Microsoft solution to encrypted config files? (when you can't use AD)

Same concept as this one, but I think a bit less user friendly.
http://lmgtfy.com/?q=encrypted+config+file+.net
