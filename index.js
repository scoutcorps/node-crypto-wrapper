'use strict';

const crypto = require('crypto');
const os = require('os');
const fs = require('fs');
const path = require('path');

const keyfilePath = path.join(os.homedir(), '.keys');
const keyfile = path.join(keyfilePath, 'crypto-wrapper.key');

function ensureKey() {
  if (!getKey()) {
    if (!fs.existsSync(keyfilePath)) {
      fs.mkdirSync(keyfilePath);
    }
    const key = crypto.randomBytes(4096).toString('hex');
    fs.writeFileSync(keyfile, key);
    fs.chmodSync(keyfile, '0600');
  }
}

function getKey() {
  return fs.existsSync(keyfile) && fs.readFileSync(keyfile).toString('utf8');
}

function encryptBinary(data, key = null) {
  if (!key) {
    ensureKey();
    key = getKey();
  }
  var cipher = crypto.createCipher('aes-256-ecb', key);
  return Buffer.concat([cipher.update(data), cipher.final()]);
}

function decryptBinary(data, key = null) {
  key = key || getKey();
  if (!key) {
    throw new Error("This user has no key present! The user's key will be created the first time you encrypt something.");
  }

  var decipher = crypto.createDecipher('aes-256-ecb', key);
  return Buffer.concat([decipher.update(data), decipher.final()]);
}

function encryptString(data, key = null) {
  return encryptBinary(Buffer.from(data, 'utf8'), key).toString('hex');
}

function decryptString(data, key = null) {
  return decryptBinary(Buffer.from(data, 'hex'), key).toString('utf8');
}

function encryptStringAsHexFile(data, file, key = null) {
  fs.writeFileSync(file, encryptString(data, key));
}

function encryptBinaryAsHexFile(data, file, key = null) {
  fs.writeFileSync(file, encryptBinary(data, key).toString('hex'));
}

function decryptHexFileAsString(file, key = null) {
  const data = Buffer.from(fs.readFileSync(file).toString('utf8'), 'hex');
  return decryptString(data, key);
}

function decryptHexFileAsBinary(file, key = null) {
  const data = Buffer.from(fs.readFileSync(file).toString('utf8'), 'hex');
  return decryptBinary(data, key);
}

module.exports = {
  encryptBinary,
  decryptBinary,
  encryptString,
  decryptString,
  encryptStringAsHexFile,
  encryptBinaryAsHexFile,
  decryptHexFileAsString,
  decryptHexFileAsBinary
};