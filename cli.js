#!/usr/bin/env node

'use strict';

const fs = require('fs');
const readline = require('readline');
const wrap = require('./index');

function xor(a,b) {
  return a ? !b : b;
}

function badUsage() {
  console.log(usage);
  return 1;
}

/*
    MODES:
    - Encrypt or decrypt
    - from: file vs string
    - to: file vs string
*/
const usage = '\r\nUsage:\r\n' +
  '  node-crypto-wrapper <options> <input> <output>\r\n' +
  'Options:\r\n' +
  '  <e: encrypt> | <d: decrypt>\r\n' +
  '  <f: from file> | <s: from string>\r\n' +
  '  <f: to file> | <s: to string>\r\n' +
  '  <p: password> | <u: user>\r\n' +
  'Examples:\r\n' +
  '  node-crypto-wrapper effu file1 file2 #encrypts file1 to a new file2 that only the current user can decrypt\r\n' +
  '  node-crypto-wrapper dfsu file1 #decrypts file1 to the console (if it was encrypted by the current user)\r\n' +
  '  node-crypto-wrapper esfp file1 #encrypts a string (typed at the prompt) to a new file1 using a password, recoverable by anyone with the password\r\n';

(function main() {
  const options = (process.argv[2] || '').toLowerCase();

  if (options.length !== 4) {
    return badUsage();
  }

  const doEncrypt = options.charAt(0) === 'e';
  const doDecrypt = options.charAt(0) === 'd';

  const fromFile = options.charAt(1) === 'f';
  const fromString = options.charAt(1) === 's';

  const toFile = options.charAt(2) === 'f';
  const toString = options.charAt(2) === 's';

  const withPassword = options.charAt(3) === 'p';
  const withUserKey = options.charAt(3) === 'u';

  if (!(xor(doEncrypt, doDecrypt) && xor(fromFile, fromString) && xor(toFile, toString) && xor(withPassword, withUserKey))) {
    return badUsage();
  }

  const output = function(oData) {
    if (toFile) {
      //there will be a third argument only in the case where it's to AND from file
      const file = fromFile ? process.argv[4] : process.argv[3];
      fs.writeFileSync(file, oData);
    }
    if (toString) {
      console.log(oData.toString('utf8'));
    }
  };

  const readFileAsHex = function(buf) {
    return Buffer.from(buf.toString('utf8'), 'hex');
  };

  try {
    const getFileAndFinish = function (password) {
      const file = fs.readFileSync(process.argv[3]);
      const iData = doEncrypt ? file : readFileAsHex(file);
      output(doEncrypt ? wrap.encryptBinary(iData, password).toString('hex') : wrap.decryptBinary(iData, password));
    };

    const getTextFromPromptAndFinish = function(rl, password) {
      rl.question('Enter text to ' + (doEncrypt ? 'en' : 'de') + 'crypt: ', iData => {
        console.log('');
        output(doEncrypt ? wrap.encryptString(iData, password) : wrap.decryptString(iData, password));
        rl.close();
      });
      rl._writeToOutput = str => {
        rl.output.write('*');
      };
    };

    if (fromString || withPassword) {
      const rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout
      });
      const writeToOutput = rl._writeToOutput;

      if (withPassword) {
        rl.question('Password: ', password => {
          if (fromString) {
            rl._writeToOutput = writeToOutput;
            getTextFromPromptAndFinish(rl, password);
          }
          else {
            getFileAndFinish(password);
            rl.close();
          }
        });
        rl._writeToOutput = str => {
          rl.output.write('*');
        };
      }
      else {
        getTextFromPromptAndFinish(rl, null);
      }
    }
    else {
      getFileAndFinish(null);
    }

    return 0;
  }
  catch (err) {
    console.log(usage);
    throw err;
  }
})();